#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>

#define MENSAJE 5000
/*
* Argv[1] IP
* Argv[2] Port
*/

void ToUpper(char* mensaje);

int main(int argc, char** argv){
	//Sokects y Len
	int lenS,socketS,socketC;
	char buffer[MENSAJE];
	memset(buffer,0,MENSAJE);
	
	if(argc!=3){
		printf("Argumentos Invalidos\n");
		return 1;
	}
	else{
		
		printf("----Servidor----\n");
		printf("Ip: %s\n",argv[1]);
		printf("Puerto: %s\n",argv[2]);

		int puerto = atoi(argv[2]);
		//Generamos el socket con estructura de internet y tipo de mensaje Stream
		socketS = socket(AF_INET,SOCK_STREAM,0);
		//Generamos tanto la estructura del servidor como cliente de Struct Sockaddr
		struct sockaddr_in servidor, cliente;
		//Configuracion Estructura
		//indicamos el tipo de conección, puerto del servidor.
		servidor.sin_family = AF_INET; 
		servidor.sin_addr.s_addr = inet_addr(argv[1]); 
  		servidor.sin_port = htons(puerto);
  		servidor.sin_addr.s_addr = INADDR_ANY;
		lenS = sizeof(servidor);
		//BInd, indicamos a qué puerto debe aceptar el servidor
		if(bind(socketS, (struct sockaddr *)&servidor, lenS) < 0){
    		printf("Error al asociar el puerto a la conexion\n");
    		close(socketS);
    		return 1;
  		}
  		//Llamada a listen
  		listen(socketS, 1); //Se espera la llamada al puerto.
  		printf("A la escucha en el puerto %d \n", ntohs(servidor.sin_port));
		//Llamada accept
		socklen_t lenC = sizeof(cliente);
		socketC = accept(socketS, (struct sockaddr *)&cliente, &lenC); //Esperamos una conexion
		if(socketC < 0){
			printf("Error al aceptar trafico\n");
			close(socketS);
			return 1;
		}
		printf("--Enviado--\n");
		int	bytes_r = recv(socketC, buffer, MENSAJE, 0);
			if(bytes_r < 0){
				printf("Error al recibir datos");
				close(socketS);		
				return 1;
			}
		else {
		//Tenemos el mensaje
//			printf("\n %s", buffer);
		//Enviamos el mensaje
			ToUpper(buffer);
			shutdown(socketS, SHUT_WR);
			write(socketC, buffer, MENSAJE);
			memset(buffer,0,MENSAJE);
	    	shutdown(socketS, SHUT_RDWR);
		}
		return 1;
	} 
}

void ToUpper(char* mensaje){
	int i = 0;
	char* temp = mensaje;
	while(mensaje[i]){
		temp[i] = toupper(mensaje[i]);
		i++;
	}
	strcpy(mensaje,temp);
}