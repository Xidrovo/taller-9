#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>

#define MENSAJE 5000
// argv[1] -> ip
// argv[2] -> socket
// argv[3] -> String

int main(int argc, char** argv){
	if(argc!=4){
		printf("Argumentos Invalidos\n");
		return 1;
	}
	printf("----Cliente----\n");
	printf("Ip: %s\n",argv[1]);
	printf("Puerto: %s\n",argv[2]);
	printf("Mensaje: %s\n",argv[3]);
	//Definiendo las variables
 	int socketC,puerto;
 	char buffer[MENSAJE];
 	memset(buffer,0,MENSAJE);
	puerto = atoi(argv[2]);
	 //Informacion sobre la direccion del servidor
 	struct sockaddr_in cliente;
 
	//Definicion de socket
	if ((socketC = socket(AF_INET, SOCK_STREAM, 0)) == -1){
		/* llamada a socket() */
		printf("socket error\n");
		return 1;
 	}
	memset(&cliente,0,sizeof(cliente));
	//Configuracion Estructura
	//Se define la familia, direccion y puerto del cliente		
	cliente.sin_family = AF_INET; 
	cliente.sin_addr.s_addr = inet_addr(argv[1]); 
	cliente.sin_port = htons(puerto);
	//Se trata de conectar con el servidor
	if(connect(socketC,(struct sockaddr *)&cliente, sizeof(cliente)) < 0){ 
    	printf("Error conectando con el host\n");
    	close(socketC);
    	return 1;
 	}
	strcpy(buffer,argv[3]);
	printf("--Conectado--\n");
	//Se trata de envíar el mensaje
	write(socketC, buffer, MENSAJE);
	memset(buffer,0,MENSAJE);
	int	bytes_r = recv(socketC, buffer, MENSAJE, 0);
			if(bytes_r < 0){
				printf("Error al recibir datos");
				close(socketC);		
				return 1;
			}
		else {
		//Tenemos el mensaje
			printf("%s \n", buffer);
			memset(buffer,0,MENSAJE);
	    	close(socketC);
		}
	return 1;

}